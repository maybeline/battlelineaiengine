import unittest
from battleline.maysbot.MaysBot import MaysBot
from battleline.maysbot.strategy.Strategy import Strategy
from battleline.maysbot.model.State import State


class TestMaysBot(unittest.TestCase):

    def setUp(self):
        self.bot = MaysBot()
        self.strategy = Strategy()
        self.state = State()
        self.build_current_state()

    def test_bot_a_bot_class(self):
        self.assertIsInstance(self.bot, MaysBot)

    def build_current_state(self):
        self.state.seat = "north"
        self.state.colors = ('color1', 'color2', 'color3', 'color4', 'color5', 'color6')
        self.state.hand = [['color3', 3], ['color2', 2], ['color2', 4], ['color1', 8], ['color2', 5], ['color6', 7], ['color4', 4]]
        self.state.flags.north = [[], [['color4', 10]], [], [], [], [['color3', 10]], [], [], []]
        self.state.flags.south = [[['color3', 7]], [['color4', 1]], [], [], [], [], [], [], []]
        self.state.flags.sides = {'north': [[], [['color4', 10]], [], [], [], [['color3', 10]], [], [], []],
            'south': [[['color3', 7]], [['color4', 1]], [], [], [], [], [], [], []]}
        self.state.flag_statuses = ['unclaimed', 'unclaimed', 'unclaimed', 'unclaimed', 'unclaimed', 'unclaimed', 'unclaimed', 'unclaimed',
             'unclaimed']
        self.state.opponents_last_play = {'flag': 2, 'card': ['color4', 1]}
        self.state.reply = "play 7 color2,10"

    def build_hand_colors(self):
        handColor = set(card[0] for card in self.state.hand)
        return handColor

    def test_state_seat(self):
        print self.state.flags.north
        self.assertEqual(self.state.seat, 'north')

    def test_choose_highest(self):
        self.assertEqual(self.strategy.get_moves(self.state, "choose_highest")[1], "color1,8")

    def test_gotColor3onFlag(self):
        gotColor3 = False
        for cards in self.state.flags.north:
            print cards
            for card in cards:
                print card
                if card[0] == 'color3':
                    gotColor3 = True
        self.assertTrue(gotColor3)

    def test_handColor(self):
        color_set = self.build_hand_colors()
        for i, cards in enumerate(self.state.flags.north):
            for card in cards:
                if card[0] in color_set:
                    print i
        self.assertFalse(True)


