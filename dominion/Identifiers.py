#cards
COPPER = "copper"
SILVER = "silver"
GOLD = "gold"

CURSE = "curse"

ESTATE = "estate"
DUCHY = "duchy"
PROVINCE = "province"

CELLAR = "cellar"
FEAST = "feast"
MARKET = "market"
MILITIA = "militia"
MINE = "mine"
MOAT = "moat"
REMODEL = "remodel"
SMITHY = "smithy"
VILLAGE = "village"
WORKSHOP = "workshop"
WOODCUTTER = "woodcutter"


# game sets
FIRST_GAME = "First Game"
